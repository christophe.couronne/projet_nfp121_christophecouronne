package fr.n7.cnam.nfp121.pr01;

import org.jdom2.*;
import org.jdom2.output.*;
import org.jdom2.input.*;
import java.io.*;

public class JDOMTools {

	public static void ecrire(Document document, OutputStream out) {
		try {
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, out);
		} catch (java.io.IOException e) {
			throw new RuntimeException("Erreur sur �criture : ", e);
		}
	}

	public static Document getDocument(Reader reader) throws IOException {
		try {
			@SuppressWarnings("deprecation")
			SAXBuilder builder = new SAXBuilder(true); // validant !
			return builder.build(reader);
		} catch (JDOMException e) {
			throw new RuntimeException("Erreur sur lecture XML : " + e.getMessage(), e);
		}
	}
}
