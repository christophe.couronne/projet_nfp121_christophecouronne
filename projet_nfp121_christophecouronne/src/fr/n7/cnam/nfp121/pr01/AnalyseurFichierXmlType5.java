package fr.n7.cnam.nfp121.pr01;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class AnalyseurFichierXmlType5 {

	private String nomFichier;
	private ArrayList<SimpleImmutableEntry<Position, Double>> listDonnees;

	public AnalyseurFichierXmlType5(String nomFichier) {

		this.nomFichier = nomFichier;
		ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<>();
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			// Cr�ation du document
			final DocumentBuilder builder = factory.newDocumentBuilder();
			final Document document = builder.parse(new File(nomFichier));
			// Cr�ation de l'�l�ment racine
			final Element donnees = document.getDocumentElement();
			// Cr�ation de la liste des noeuds (�l�ments enfants)
			final NodeList racineNoeuds = donnees.getChildNodes();
			final int nbRacineNoeuds = racineNoeuds.getLength();
			// Boucle sur chaque �l�ment enfant
			for (int i = 0; i < nbRacineNoeuds; i++) {
				if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
					// R�cup�ration de l'�l�ment enfant
					final Element elemDonnee = (Element) racineNoeuds.item(i);
					//R�cup�ration des �l�ments petits-enfants par tag-name
					final Element elemX = (Element) elemDonnee.getElementsByTagName("y").item(0);
					final Element elemY = (Element) elemDonnee.getElementsByTagName("x").item(0);
					// R�cup�ration des valeurs
					int x = Integer.parseInt(elemX.getTextContent());
					int y = Integer.parseInt(elemY.getTextContent());
					Position p = new Position(x, y);
					double valeur = Double.parseDouble(elemDonnee.getAttribute("valeur"));
					// Cr�ation de la liste de donn�es
					SimpleImmutableEntry<Position, Double> entree = new SimpleImmutableEntry<Position, Double>(p, valeur);
					source.add(entree);
				}
				this.listDonnees = source;
			}
		} catch (

		final ParserConfigurationException e) {
			e.printStackTrace();
		} catch (final SAXException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getNomFichier() {
		return this.nomFichier;
	}
	
	public ArrayList<SimpleImmutableEntry<Position, Double>> getListDonnees() {
		return this.listDonnees;
	}

}
