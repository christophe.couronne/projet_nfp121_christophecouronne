package fr.n7.cnam.nfp121.pr01;

import java.lang.reflect.*;
import java.util.*;

/**
 * TraitementBuilder
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class TraitementBuilder {

	/**
	 * Retourne un objet de type Class correspondant au nom en param�tre. Exemples :
	 * - int donne int.class - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {

		if (nomType.contains("int")) {
			return int.class;
		} else if (nomType.contains("double")) {
			return double.class;
		} else {
			return Class.forName(nomType);
		}
	}

	/**
	 * Cr�e l'objet java qui correspond au type formel en exploitant le � mot �
	 * suviant du scanner. Exemple : si formel est int.class, le mot suivant doit
	 * être un entier et le résulat est l'entier correspondant. Ici, on peut se
	 * limiter aux types utlisés dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {
		String testValue = in.next();
		if (formel.equals(int.class)) {
			return Integer.parseInt(testValue);
		} else if (formel.equals(double.class)) {
			return Double.parseDouble(testValue);
		} else {
			return formel.cast(testValue);
		}
	}

	/**
	 * D�finition de la signature, les param�tres formels, mais aussi les param�tres
	 * formels.
	 */
	static class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/**
	 * Analyser une signature pour retrouver les param�tres formels et les
	 * param�tres effectifs. Exemple � 3 double 0.0 String xyz int -5 � donne -
	 * [double.class, String.class, int.class] pour les param�tres effectifs et -
	 * [0.0, "xyz", -5] pour les param�tres formels.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		// R�cupartion du premier mot comme nombre de param�tre
		int nbParametre = in.nextInt();

		Class<?>[] tabClassType = new Class<?>[nbParametre];
		Object[] tabObjectValue = new Object[nbParametre];

		for (int i = 0; i < nbParametre; i++) {
			// R�cup�ration type du param�tre
			tabClassType[i] = analyserType(in.next());
			Object objectValeur = decoderEffectif(tabClassType[i], in);
			// R�cup�ration de la valeur du param�tre
			tabObjectValue[i] = objectValeur;
		}
		return new Signature(tabClassType, tabObjectValue);
	}

	/**
	 * Analyser la cr�ation d'un objet. Exemple : � Normaliseur 2 double 0.0 double
	 * 100.0 � consiste � charger la classe Normaliseur, trouver le constructeur qui
	 * prend 2 double, et l'appeler en lui fournissant 0.0 et 100.0 comme param�tres
	 * effectifs.
	 */
	Object analyserCreation(Scanner in) throws ClassNotFoundException, InvocationTargetException,
			IllegalAccessException, NoSuchMethodException, InstantiationException {

		// R�cup�ration du nom du traitement
		String nomTraitement = "fr.n7.cnam.nfp121.pr01." + in.next();
		// Cr�ation de la classe � partir du nom
		Class<?> classTraitement = Class.forName(nomTraitement);
		// Cr�ation de la signature du traitement
		Signature signature = analyserSignature(in);
		// Cr�ation du traitement
		Constructor constructeur = classTraitement.getConstructor(signature.formels);
		Object traitement = constructeur.newInstance(signature.effectifs);

		return traitement;
	}

	/**
	 * Analyser un traitement. Exemples : - � Somme 0 0 � - � SupprimerPlusGrand 1
	 * double 99.99 0 � - � Somme 0 1 Max 0 0 � - � Somme 0 2 Max 0 0
	 * SupprimerPlusGrand 1 double 20.0 0 � - � Somme 0 2 Max 0 0 SupprimerPlusGrand
	 * 1 double 20.0 1 Positions 0 0 �
	 * 
	 * @param in  le scanner � utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env) throws ClassNotFoundException,
			InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException {

		Traitement traitement = (Traitement) analyserCreation(in);

		// Mise en place d'un boucle r�cursive en fonction du nombre de traitements
		int nbTraitementSuivant = in.nextInt();
		for (int i = 0; i < nbTraitementSuivant; i++) {
			traitement.ajouterSuivants(analyserTraitement(in, env));
		}
		return traitement;
	}

	/**
	 * Analyser un traitement.
	 * 
	 * @param in  le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env) {
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, " + "voir la cause ci-dessous", e);
		}
	}

}
