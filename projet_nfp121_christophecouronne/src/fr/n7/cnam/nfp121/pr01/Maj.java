package fr.n7.cnam.nfp121.pr01;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées) lors
 * du traitement de ce lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	private Lot lot;

	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.lot = new Lot(nomLot);
	}

	@Override
	public void traiter(Position position, double valeur) {
		boolean present = false;
		// Condition lot n'est pas vide
		if (this.lot.getDonnees().size() != 0) {
			for (Donnee donnee : this.lot.getDonnees()) {
				// Condition la position existe dans lot
				if(position.getX() == donnee.getPosition().getX() && position.getY() == donnee.getPosition().getY()) {
					present = true;
				}
			}
		}
		if(!present) {
			// Condition position absente de lot > Ajout � lot
			this.lot.traiter(position, valeur);
		}else {
			// Condition position pr�sente dans lot > MAJ valeur
			this.lot.setValeurParPosition(position, valeur);
		}
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		for (Donnee donnee : this.lot.getDonnees()) {
			System.out.println(nomLot + ": position mise � jour au cours du tratement = (" + donnee.getPosition().getX()
					+ "," + donnee.getPosition().getY()+ ") > "+donnee.getValeur());
		}

	}

}
