package fr.n7.cnam.nfp121.pr01;

import java.util.List;

/**
 * Normaliseur normalise les donnÃ©es d'un lot en utilisant une transformation
 * affine.
 *
 * @author Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
 */
public class Normaliseur extends Traitement {

	private double debut, fin;
	private Max max;
	private Donnees donnees;

	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
	}

	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.max = new Max();
		this.donnees = new Donnees();
	}

	@Override
	public void traiter(Position position, double valeur) {
		this.max.traiter(position, valeur);
		this.donnees.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		double max = this.max.max();
		double min = -max;
		double a = (max - min) / (this.fin - this.debut);
		double b = this.debut - a*min;
		List<Donnee> donnees = this.donnees.donnees();
		for (Donnee donnee : donnees) {
			donnee.setValeur((a*donnee.getValeur())+b);
			super.traiter(donnee.getPosition(), donnee.getValeur());
		}
	}
}
