package fr.n7.cnam.nfp121.pr01;

public class CycleException extends Exception{
	
	public CycleException() {
		System.out.println("CycleException : Un traitement revient en boucle");
		// Une boucle de traitement va cr�er un StackOverflowError
		System.out.println("Arr�t du programme");
		System.exit(1);
	}
	
}
