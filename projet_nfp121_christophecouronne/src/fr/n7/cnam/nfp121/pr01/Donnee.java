package fr.n7.cnam.nfp121.pr01;

public class Donnee {

	private Position position;
	private double valeur;

	public Donnee(Position position, double valeur) {
		this.position = position;
		this.valeur = valeur;
	}
	
	public Position getPosition() {
		return this.position;
	}
	
	public double getValeur() {
		return this.valeur;
	}
	
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

	public void displayDetails() {
		System.out.println("(" + this.position.getX() + "," + this.position.getY() + ") > "+ this.getValeur());
	}
	
}
