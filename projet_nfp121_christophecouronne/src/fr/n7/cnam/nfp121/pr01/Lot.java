package fr.n7.cnam.nfp121.pr01;

import java.util.List;

public class Lot extends Traitement {

	private String nomLot;
	private Donnees donnees;
	
	public Lot(String nomLot) {
		this.nomLot = nomLot;
		this.donnees = new Donnees();
	}
	
	public void traiter(Position position, double valeur) {
		Donnee donnee = new Donnee (position, valeur);
		this.donnees.donnees().add(donnee);
		super.traiter(position, valeur);
	}
	
	public void setValeurParPosition(Position position, double valeur) {
		for (Donnee donnee : donnees.donnees()) {
			if(donnee.getPosition().equals(position)) {
				donnee.setValeur(valeur);
			}
		}
	}
	
	public String getNom() {
		return this.nomLot;
	}
	
	public List<Donnee> getDonnees() {
		return this.donnees.donnees();
	}

}
