package fr.n7.cnam.nfp121.pr01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleImmutableEntry;

public class AnalyseurFichierTxtType2 {

	private String nomFichier;
	private ArrayList<SimpleImmutableEntry<Position, Double>> listDonnees;

	public AnalyseurFichierTxtType2(String nomFichier) {
		this.nomFichier = nomFichier;

		ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<>();

		try (BufferedReader in = new BufferedReader(new FileReader("src/"+nomFichier))) {

			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 6; // 6 mots sur chaque ligne

				int x = Integer.parseInt(mots[1]);
				int y = Integer.parseInt(mots[2]);
				Position p = new Position(x, y);
				double valeur = Double.parseDouble(mots[4]);

				SimpleImmutableEntry<Position, Double> entree = new SimpleImmutableEntry<Position, Double>(p, valeur);
				source.add(entree);
			}
			this.listDonnees = source;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public String getNomFichier() {
		return this.nomFichier;
	}
	
	public ArrayList<SimpleImmutableEntry<Position, Double>> getListDonnees() {
		return this.listDonnees;
	}
	
}
