package fr.n7.cnam.nfp121.pr01;

import java.util.ArrayList;
import java.util.List;

/**
  * Donnees enregistre toutes les donn�es re�ues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	private List<Donnee> donnees = new ArrayList<>();
	
	@Override
	public void traiter(Position position, double valeur) {
		Donnee donnee = new Donnee (position, valeur);
		this.donnees.add(donnee);
		super.traiter(position, valeur);
	}
	
	public List<Donnee> donnees() {
		return this.donnees;
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		
		System.out.println(nomLot + ": nb donnees = " + this.donnees.size());
	}

}
