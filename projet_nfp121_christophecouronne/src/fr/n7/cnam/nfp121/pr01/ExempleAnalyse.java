package fr.n7.cnam.nfp121.pr01;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.AbstractMap.SimpleImmutableEntry;

/**
 * ExempleAnalyse
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class ExempleAnalyse {

	public static void exemple1() {
		System.out.println();
		System.out.println("=== exemple1() ===");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		// *** Construire le traitement
		SommeAbstrait somme = traitements.somme();
		SommeAbstrait somme2 = traitements.somme();
		PositionsAbstrait positions = traitements.positions();
		Donnees donnees = traitements.donnees();
		Multiplicateur multiplicateur = traitements.multiplicateur(10);
		SommeParPosition sommeParPosition = traitements.sommeParPosition();
		SupprimerPlusGrand supprimerPlusGrand = traitements.supprimerPlusGrand(4);
		SupprimerPlusPetit supprimerPlusPetit = traitements.supprimerPlusPetit(0);
		Max max = traitements.max();
		Normaliseur normaliseur = traitements.normaliseur(2, 4);
		GenerateurXML generateurXML = traitements.generateurXML("MonFichierTest_GenerateurXML");
		Maj maj = traitements.maj();
		Moyenne moyenne = traitements.moyenne();

		// *** Ajout de traitements au traitement initial
		// Test 1.1
//		somme.ajouterSuivants(somme); // Exception catch�e
		// Test 1.2
//		somme.ajouterSuivants(somme2); // Traitement r�alis�
		// Test 2
//		somme.ajouterSuivants(positions);
//		positions.ajouterSuivants(somme); // Exception catch�e
		// Test 3
//		positions.ajouterSuivants(somme);
//		somme.ajouterSuivants(donnees);
//		donnees.ajouterSuivants(somme);// Exception catch�e
		// Test 4
		somme.ajouterSuivants(maj);
		maj.ajouterSuivants(normaliseur);
        somme.ajouterSuivants(maj);

		// *** Initialisation analyseur
		Analyseur analyseur = new Analyseur(somme);

		// *** Traiter des données manuelles
		somme.gererDebutLot("LotTest");
		somme.traiter(new Position(1, 1), 5.0);
		somme.traiter(new Position(1, 2), 2.0);
		somme.traiter(new Position(1, 1), -1.0);
		somme.traiter(new Position(1, 2), 1.5);
		somme.gererFinLot("LotTest");

		// *** R�sultat traitement somme
//		System.out.println("Somme = " + somme.somme());
		// *** R�sultat traitement Positions
//		System.out.println("Positions.frequence(new Position(1,2)) = " + positions.frequence(new Position(1, 2)));
		// *** R�sultat traitement donnees
//		for (Donnee donnee : donnees.donnees()) {
//			donnee.displayDetails();
//		}

	}

	public static void exemple2(String traitements) throws FileNotFoundException {

		System.out.println();
		System.out.println("=== exemple2(" + traitements + ") ===");

		// Construire les traitements
		TraitementBuilder builder = new TraitementBuilder();
		Traitement main = builder.traitement(new java.util.Scanner(traitements), null);

		System.out.println("Traitement : " + main);

		// Traiter des données manuelles
		main.gererDebutLot("manuelles");
		main.traiter(new Position(1, 1), 5.0);
		main.traiter(new Position(1, 2), 2.0);
		main.traiter(new Position(1, 1), -1.0);
		main.gererFinLot("manuelles");

		// Construire l'analyseur
		Analyseur analyseur = new Analyseur(main);

		// Traiter les autres sources de données : "donnees.txt", etc.
	}

	public static void exemple3() {
		System.out.println();
		System.out.println("=== exemple3() ===");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		// *** Construire le traitement
		Donnees donnees = traitements.donnees();
		Donnees donnees2 = traitements.donnees();
		Donnees donnees3 = traitements.donnees();
		GenerateurXML generateurXML = traitements
				.generateurXML("MonFichierTest_GenerateurXML_fromMonFichierTest_SaisiesSwing");

		// R�cup�ration des donn�es
//		AnalyseurFichierTxtType1 fichier = new AnalyseurFichierTxtType1("donnees.txt");
		AnalyseurFichierTxtType1 fichier = new AnalyseurFichierTxtType1("MonFichierTest_SaisiesSwing.txt");
//		AnalyseurFichierTxtType2 fichier = new AnalyseurFichierTxtType2("donnees2-f2.txt");
//		AnalyseurFichierXmlType5 fichier = new AnalyseurFichierXmlType5("src/donnees5.xml");

		donnees.ajouterSuivants(generateurXML);
		generateurXML.ajouterSuivants(donnees2);
		donnees2.ajouterSuivants(donnees3);

		// *** Initialisation analyseur
		Analyseur analyseur = new Analyseur(donnees);
		analyseur.traiter(fichier.getListDonnees(), fichier.getNomFichier());

		System.out.println("Traitement : " + donnees);

		// *** R�sultat traitement donnees
		for (Donnee donnee : donnees.donnees()) {
			donnee.displayDetails();
		}
	}

	public static void main(String[] args) throws java.io.FileNotFoundException {
		exemple1();

//		exemple2("Somme 0 1 Positions 0 0");

		String calculs = "Positions 0 1 Max 0 1 Somme 0 1 SommeParPosition 0";
		String generateur = "GenerateurXML 1 java.lang.String NOM--genere.xml";
		String traitement1 = generateur.replaceAll("NOM", "brut") + " 3" + " " + calculs + " 0" + " "
				+ "SupprimerPlusPetit 1 double 0.0 1 SupprimerPlusGrand 1 double 10.0 2" + " "
				+ generateur.replaceAll("NOM", "valides") + " 0" + " " + calculs + " 0" + " "
				+ "Normaliseur 2 double 0.0 double 100.0 2" + " " + generateur.replaceAll("NOM", "normalisees") + " 0"
				+ " " + calculs + " 0";

//		exemple2(calculs + " 0");
//		exemple2(traitement1);

//		exemple3();
	}

}
