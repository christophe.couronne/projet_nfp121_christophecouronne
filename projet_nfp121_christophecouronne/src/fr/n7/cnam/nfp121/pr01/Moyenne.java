package fr.n7.cnam.nfp121.pr01;

import java.util.ArrayList;
import java.util.List;

public class Moyenne extends Traitement {

	private List<Lot> lotList = new ArrayList<>();
	private Lot lot;

	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.lot = new Lot(nomLot);
	}

	@Override
	public void traiter(Position position, double valeur) {
		this.lot.traiter(position, valeur);
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {

		// Ajout du lot � la liste de lots
		lotList.add(this.lot);
		
		int nbValeurs = 0;
		double totalValeurs = 0;
		int nbValeursTotal = 0;
		double totalValeursTotal = 0;
		
		for (Lot lotIt : lotList) {
			for (Donnee donnee : lotIt.getDonnees()) {
				if(lotIt.getNom() == this.lot.getNom()) {
					nbValeurs++;
					totalValeurs += donnee.getValeur();
				}
				nbValeursTotal++;
				totalValeursTotal += donnee.getValeur();
			}
		}

		System.out.println(nomLot + ": moyenne = "+(totalValeurs/nbValeurs));
		System.out.println("TotalLots: moyenne = "+(totalValeursTotal/nbValeursTotal));
		
	}

}
