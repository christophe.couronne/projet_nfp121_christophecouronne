package fr.n7.cnam.nfp121.pr01;

/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double maxValeur;
	private boolean init;
	
	public Max() {
		init = true;
	}
	
	public void traiter(Position position, double valeur) {
		if (init) {
			maxValeur = valeur;
			init = false;
		}else if(valeur > this.maxValeur) {
			maxValeur = valeur;
		}
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		// Affichage format : lot1: max = 18.0
		System.out.println(nomLot + ": max = " + this.maxValeur);
	}
	
	public double max() {
		return this.maxValeur;
	}

}
