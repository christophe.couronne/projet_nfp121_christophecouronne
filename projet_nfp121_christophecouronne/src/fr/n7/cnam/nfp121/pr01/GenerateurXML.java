package fr.n7.cnam.nfp121.pr01;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.*;
import org.jdom2.output.*;

/**
 * GenerateurXML Ã©crit dans un fichier, Ã  charque fin de lot, toutes les
 * donnÃ©es lues en indiquant le lot dans le fichier XML.
 *
 * @author Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {

	// produit à chaque ﬁn de lot, un document XML avec toutes les données traitées
	// (de ce lot et des précédents) organisées par lot
	// (i.e.le lot d’appartenance d’une donnée doit se retrouver dans le document
	// XML).
	// Le nom du ﬁchier dans lequel le document sera écrit sera un paramètre du
	// constructeur de ce traitement.
	// La DTD de ce document est à déﬁnir. Elle doit s’appeler generateur.dtd.

	private String nomDocument;
	private List<Lot> lotList = new ArrayList<>();
	private Lot lot;

	public GenerateurXML(String nomDocument) {
		this.nomDocument = nomDocument;
	}

	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.lot = new Lot(nomLot);
	}

	@Override
	public void traiter(Position position, double valeur) {
		lot.traiter(position, valeur);
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {

		// Ajout du lot à la liste de lots
		lotList.add(lot);

		Element generateur = new Element(this.nomDocument);
		// Création arborescence xml
		for (Lot lot : lotList) {

			// Création élément lot
			Element elemLot = new Element("lot");
			generateur.addContent(elemLot);
			Attribute elemLotAttribute = new Attribute("nom", lot.getNom());
			elemLot.setAttribute(elemLotAttribute);

			// Création données du lot
			for (Donnee donnee : lot.getDonnees()) {
				// Création donnée
				Element elemDonnee = new Element("donnee");
				elemLot.addContent(elemDonnee);
				Attribute donneeValeur = new Attribute("valeur", String.valueOf(donnee.getValeur()));
				elemDonnee.setAttribute(donneeValeur);
				// Création Y
				Element y = new Element("y");
				elemDonnee.addContent(y);
				y.setText(String.valueOf(donnee.getPosition().getY()));
				// Création X
				Element x = new Element("x");
				elemDonnee.addContent(x);
				x.setText(String.valueOf(donnee.getPosition().getX()));
			}
		}

		try {
			Document document = new Document(generateur, new DocType(this.nomDocument, "src/generateur.dtd"));
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
//			sortie.output(document, System.out);
			File fichier = new File("src/"+this.nomDocument+".xml");
			if (fichier.exists()) {
				FileWriter fw = new FileWriter(fichier, true);
				fw.write(sortie.outputString(generateur));
				fw.flush();
				fw.close();
			} else {
				
				sortie.output(document, new FileOutputStream("src/"+this.nomDocument+".xml"));
			}
		} catch (IOException e) {
			System.out.println("IOException : " + e);
		}

	}

}
