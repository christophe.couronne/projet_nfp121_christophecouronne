package fr.n7.cnam.nfp121.pr01;

import java.util.*;

/**
 * SommeParPosition
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class SommeParPosition extends Traitement {

	private HashMap<Position, Double> mapPositionsSommes = new HashMap<Position, Double>();

	@Override
	public void traiter(Position position, double valeur) {
		if (!mapPositionsSommes.containsKey(position)) {
			// Si la position n'est pas contenue dans la map, le couple (position; valeur)
			// est ajout�
			mapPositionsSommes.put(position, valeur);
		} else {
			// Si la position est pr�sente, on ajoute � la valeur donn�e la valeur dans la map
			// et le coupe (position; valeur) est remplac�
			double somme = mapPositionsSommes.get(position) + valeur;
			mapPositionsSommes.put(position, somme);
		}
		super.traiter(position, valeur);
	}

	public double sommeParPosition(Position position) {
		return this.mapPositionsSommes.get(position);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		int i = 1;
		// Affichage format : SommeParPosition lot1 :
					System.out.println("SommeParPosition "+nomLot+" :");
		for (Map.Entry<Position, Double> donnee : mapPositionsSommes.entrySet()) {
			// Affichage format : - Position@400(2,1) -> 18.0 
			System.out.println("- Position@"+i+"("+donnee.getKey().getX()+","+donnee.getKey().getY()+") -> "+donnee.getValue());
		}
		// Affichage format : Fin SommeParPosition
					System.out.println("Fin SommeParPosition");
			
	}
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		System.out.println("DEBUT SOMME PAR POSITION");
	}

}
