package fr.n7.cnam.nfp121.pr01;

import java.util.*;

/**
 * Positions enregistre toutes les positions, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class Positions extends PositionsAbstrait {

	private ArrayList<Position> positions = new ArrayList<Position>();

	/**
	 * Obtenir le nombre de positions m�moris�es.
	 */
	@Override
	public int nombre() {
		return positions.size();
	}

	/**
	 * Obtenir la i�me position enregistr�e.
	 */
	@Override
	public Position position(int indice) {
		return this.positions.get(indice);
	}

	/** Obtenir la fr�quence d'une position dans les positions trait�es.
	 */
	@Override
	public int frequence(Position position) {
		int frequence = Collections.frequency(this.positions, position);
		return frequence;
	}
	
	/**
	 * Ajoute la position dans la liste de positions
	 */
	@Override
	public void traiter(Position position, double valeur) {
		positions.add(position);
		super.traiter(position, valeur);
	}

}
