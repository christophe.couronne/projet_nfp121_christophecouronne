package fr.n7.cnam.nfp121.pr01;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleImmutableEntry;

import javax.swing.*;

public class SaisiesSwing {
	
	private String nomFichier;
	private ArrayList<SimpleImmutableEntry<Integer, Donnee>> listDonnees = new ArrayList<>();
	int i = 1;

	public SaisiesSwing(String nomFichier) {

		this.nomFichier = nomFichier+".txt";
		vueSwing();

	}

	public void vueSwing() {
		// Fen�tre contenant
				JFrame fenetre = new JFrame("Saisie donn�es");
				fenetre.setSize(400, 120);
				fenetre.setLocation(200, 100);
				fenetre.setLayout(new BorderLayout());

				// Labels
				JPanel labels = new JPanel(new GridLayout(1, 3));
				fenetre.add(labels, BorderLayout.NORTH);
				JLabel labelAbscisse = new JLabel("Abscisse");
				labelAbscisse.setHorizontalAlignment(JLabel.CENTER);
				labels.add(labelAbscisse);
				JLabel labelOrdonnee = new JLabel("Ordonn�e");
				labelOrdonnee.setHorizontalAlignment(JLabel.CENTER);
				labels.add(labelOrdonnee);
				JLabel labelValeur = new JLabel("Valeur");
				labelValeur.setHorizontalAlignment(JLabel.CENTER);
				labels.add(labelValeur);

				// Texts input
				JPanel texts = new JPanel(new GridLayout(1, 3));
				fenetre.add(texts, BorderLayout.CENTER);
				JTextField textAbscisse = new JTextField();
				textAbscisse.setHorizontalAlignment(JTextField.CENTER);
				texts.add(textAbscisse);
				JTextField textOrdonnee = new JTextField();
				textOrdonnee.setHorizontalAlignment(JTextField.CENTER);
				texts.add(textOrdonnee);
				JTextField textValeur = new JTextField();
				textValeur.setHorizontalAlignment(JTextField.CENTER);
				texts.add(textValeur);

				// Boutons
				JPanel boutons = new JPanel(new FlowLayout());
				fenetre.add(boutons, BorderLayout.SOUTH);
				JButton btnValider = new JButton("Valider");
				btnValider.addActionListener(ev -> {
					actionValider(textAbscisse, textOrdonnee, textValeur);
				});
				boutons.add(btnValider);
				JButton btnEffacer = new JButton("Effacer");
				btnEffacer.addActionListener(ev -> {
					actionEffacer(textAbscisse, textOrdonnee, textValeur);
				});
				boutons.add(btnEffacer);
				JButton btnTerminer = new JButton("Terminer");
				btnTerminer.addActionListener(ev -> {
					actionTerminer();
				});
				boutons.add(btnTerminer);

				fenetre.setVisible(true); // la rendre visible
	}
	
	public void actionEffacer(JTextField... textFields) {
		for (JTextField textfield : textFields) {
			textfield.setText(null);
			textfield.setBackground(Color.WHITE);
		}
	}

	public void actionValider(JTextField textAbscisse, JTextField textOrdonnee, JTextField textValeur) {
		int abscisse = 0, ordonnee = 0;
		double valeur = 0;
		boolean go = true;
		
		try {
			abscisse = Integer.parseInt(textAbscisse.getText());
			restaureWhite(textAbscisse);
		} catch (NumberFormatException e) {
			go = erreurCatch(textAbscisse);
		}
		try {
			ordonnee = Integer.parseInt(textOrdonnee.getText());
			restaureWhite(textOrdonnee);
		} catch (NumberFormatException e) {
			go = erreurCatch(textOrdonnee);
		}
		try {
			valeur = Double.parseDouble(textValeur.getText());
			restaureWhite(textValeur);
		} catch (NumberFormatException e) {
			go = erreurCatch(textValeur);
		}
		if (go) {
			Position p = new Position(abscisse, ordonnee);
			Donnee d = new Donnee (p, valeur);
			SimpleImmutableEntry<Integer, Donnee> entree = new SimpleImmutableEntry<Integer, Donnee>(this.i, d);
			this.listDonnees.add(entree);
			System.out.println("Nouvelle entree "+this.i+" : (" + abscisse + "," + ordonnee + ") > " + valeur);
			actionEffacer(textAbscisse, textOrdonnee, textValeur);
			i++;
		}
		
	}
	
	public void actionTerminer() {
		PrintWriter writer;
		try {
			writer = new PrintWriter("src/"+nomFichier, "UTF-8");
			for (SimpleImmutableEntry<Integer, Donnee> entree : listDonnees) {
				int index = entree.getKey();
				int abscisse = entree.getValue().getPosition().getX();
				int ordonnee = entree.getValue().getPosition().getY();
				double valeur = entree.getValue().getValeur();
				writer.println(abscisse+" "+ordonnee+" "+index+" "+valeur);
			}
			writer.close();
			System.out.println("Fichier "+nomFichier+" cr��");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			System.out.println("Erreur export donnees : "+e);
			e.printStackTrace();
		}
		System.exit(1);
	}
	
	private boolean erreurCatch(JTextField textField) {
		textField.setBackground(Color.RED);
		return false;
	}
	
	private void restaureWhite(JTextField textField) {
		textField.setBackground(Color.WHITE);
	}

	// For TEST
	public static void main(String[] args) {
		new SaisiesSwing("MonFichierTest_SaisiesSwing");
	}
}
