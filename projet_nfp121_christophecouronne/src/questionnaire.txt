% NOM Prénom (numéro) : COURONNE Christophe (5)


**Remarque :** Ce fichier est au format Markdown.  À partir de lui, on peut
produire un pdf, un odt, un docx, du html, etc.  Par exemple, avec pandoc on
pourra faire :

~~~
pandoc --toc -o questionnaire.pdf questionnaire.txt
pandoc --toc -s -o questionnaire.html questionnaire.txt
~~~


# Points non traités du sujet

Tous les points ont �t� trait�s.



# Traitements



## Quelle est la classe du traitement choisi ?

Classe Traitement



## Pourquoi avoir fait de traitement une classe abstraite et non une interface ?

Une interface force � r��crire toutes les m�thodes.
Ici on a besoin de ne r��crire que certaines m�thodes et d'h�riter des autres.
Aussi Traitement ne doit pas �tre instanci�. Seules les classes concr�tes en d�coulant ne douvent l'�tre.



## Pourquoi certaines méthodes sont déclarées `final` ?

Cela �vite qu'elle puisse �tre red�finie dans une classe d�riv�e.



## La classe Traitement est abstraite alors qu'elle ne contient pas de méthodes abstraites. Pourquoi ?

La classe est abstraite pour �tre red�finie mais les m�thodes ne doivents pas obligatoirement �tre red�finies.



## Est-ce que de faire de Traitement une classe abstraite est vraiment logique ici ?

Oui ca cela impose la cr�ation de m�thodes concr�tes qui h�ritent des m�thodes d�finient dans Traitement



## Pour le traitement Normaliseur, quels autres traitements avez-vous utilisés et comment ?

Pour Normalisateur, j'ai utilis� Max et Donnees.
On aurait pu cr�er un traitement transfoAffine qui prendrait 4 param�tres (d�but, fin, max, min).
Normaliseur appellerait traiter() de transfoAffine et enregistrerait les valeurs mises � jour au niveau de traiter() de Normaliseur.



## Quelles modifications avez-vous été obligés de faire sur la classe Position ?

equals a du �tre red�finit pour tester les valeurs et pas l'objet lui-m�me.



# Remarques sur Swing

vueSwing() correspond � la partie vue qui pr�sente les �l�ments visibles.
Les listeneurs sur les boutons pourraient �tre envisag�s dans une partie controleur.
L'actionEffacer se passe simplement dans une boucle puisque c'est une m�me action � faire sur les 3 champs
L'actionValider utilise des try/catch pour g�rer unitairement les erreurs de saisie (y compris la validation � null).
Cela permet de corriger les valeurs sans avoir � effacer l'ensemble ou perdre les valeurs saisies.
Aussi la validation ajoute chaque donn�e dans un ArrayList attrubut de classe.
L'actionTerminer g�n�re un fichier txt au format demand� ligne par ligne (via une boucle)
Ce traitement est n�cessairement plac� dans un try/catch.



# Remarques sur l'Introspection

Le scanner passe mot � mot la string qui lui a �t� donn�.
Donc le premier mot est le premier traitement � r�cup�rer dans analyserCreation.
Class.forName(nomTraitement) permet de r�cup�rer la classe proprement dite.
La suite du scanner donne le nombre de param�tres r�cup�r� dans analyserSignature.
Ensuite dans une boucle on r�cup�re les param�tres 2 � 2 : le type et la valeur.
Le type est r�cup�r� dans analyserType � partir d'une string et retourne une Class<?>.
La valeur est r�cup�r�e dnas decoderEffectif � partir du type r�cup�r� et du scanner.
Le type et la valeur sant plac�s dans des tableaux qui ensuite permettent de cr�er une Signature.
Cette signature est retourn�e � analyserCreation.
On obtient un constructeur grace � la m�thode .getConstructor de la classe et aux param�tres formels
Puis on peut appeler newInstance de notre constructeur, avec les paramettres effectifs
Ce qui nous permet de r�cup�rer un objet "g�n�rique".
Cet objet est transtip� dans analyserTraitement pour obtenir un Traitement.
Le mot suivant du scanner donne le nombre de traitement attendu.
En fonction de ce nombre on lance une boucle qui appelle la m�thode analyserTraitement de fa�on r�cursive.
cela permet d'ajouter les traitements suivants.



# Remarques sur XML

## Lecture d'un document XML

AnalyseurFichierXmlType5 prend en param�tre le nom du fichier � lire
Un DocumentBuilderFactory est mis en place pour lire le fichier.
puis on appelle la fonction .newDocumentBuilder() pour obtenir un lecteur du document
La methode .parse du DocumentBuider permet de r�cup�rer tout le document.
Ensuite on lit les �l�ments niveau par niveau.
D'abord la racine du document avec un getDocumentElement().
Puis on r�cup�re les noeuds enfants avec .getChildNodes();
En fonction du nombre de noeuds, on lance une boucle dans laquelle on va r�cup�rer chaque �l�ment 
On peut ainsi cr�er les Donn�es avec les Positions et les Valeurs
et les ajouter dans un ArrayList.
La lecture du fichier est fonction de dtd de type 5.
C'est � dire en respectant l'architecture impos�e par ce fichier.
donnees5.xml est un fichier donn� � tritre d'exemple.

## Production d'un document XML

Comme pour la lecture, on avance niveau par niveau.
On cr�e des �l�ments que l'on adjoint � l'�l�ment parent.
L'architecture du document control� par le fichier dtd generateur.dtd



# Principaux choix faits

Pour la gestion des Lots, un traitement Lot a �t� cr�� ce qui permet de de stocker � la fois les Donn�e mais aussi le nom du lot associ�.



# Critiques de l'architecture proposée et améliorations possibles

...


# Difficultés rencontrées

La principale difficult�e a �t� la compr�hension d�taill�e des besoins du client
et de l'architecture pr�-impos�e (d�crite dans la documentation).
Ce qui semble limpide pour le r�dacteur peut �tre plus flou, avoir moins de sens, pour le lecteur.


